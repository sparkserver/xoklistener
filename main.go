package main

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis/v8"
	"github.com/spf13/viper"
)

type PersonaInfo struct {
	Name string
	ID uint64
	UserID uint64
}

type BanNotification struct {
	Persona PersonaInfo
	BannedBy PersonaInfo
	Reason *string
	EndsAt *string
}

var dgo, _ = discordgo.New()

func main() {
	viper.SetDefault("redis.addr", "localhost:6379")
	viper.AddConfigPath(".")
	viper.SetConfigName("xoklistener")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Failed to read config: %v", err)
	}

	rdb := redis.NewClient(&redis.Options{
		Addr: viper.GetString("redis.addr"),
	})
	if err := rdb.Ping(context.Background()).Err(); err != nil {
		log.Fatalf("Failed to connect to redis: %v", err)
	}
	pubsub := rdb.Subscribe(context.Background(), "ban")
	for msg := range pubsub.Channel() {
		log.Printf("Received message: %v", msg)
		switch msg.Channel {
		case "ban":
			HandleBanNotification([]byte(msg.Payload))
		}
	}
}

func HandleBanNotification(payload []byte) {
	msg := new(BanNotification)
	if err := json.Unmarshal(payload, msg); err != nil {
		log.Printf("Failed to unmarshal ban notification: %v", err)
	}

	fields := []*discordgo.MessageEmbedField{
		{
			Name: "Player",
			Value: msg.Persona.Name,
		},
	}
	if msg.Reason != nil {
		fields = append(fields, &discordgo.MessageEmbedField{
			Name: "Reason",
			Value: *msg.Reason,
		})
	}

	_, err := dgo.WebhookExecute(
		viper.GetString("webhook.id"),
		viper.GetString("webhook.token"),
		false,
		&discordgo.WebhookParams{
			Embeds: []*discordgo.MessageEmbed{
				{
					Title: "🔨 Player banned",
					Timestamp: time.Now().UTC().Format(time.RFC3339),
					Fields: fields,
					Color: 0xFF0000,
				},
			},
		},
	)
	if err != nil {
		log.Printf("Failed to execute webhook: %v", err)
	}
}
