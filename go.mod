module gitlab.com/sparkserver/xoklistener

go 1.14

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/go-redis/redis/v8 v8.3.3
	github.com/spf13/viper v1.7.1
)
